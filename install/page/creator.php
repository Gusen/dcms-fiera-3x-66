<?php

include_once H . 'sys/inc/shif.php';
$time = time();
include_once H . 'sys/inc/db_connect.php';
if (!isset($_SESSION['shif'])) $_SESSION['shif'] = $passgen;
if (isset($_SESSION['adm_reg_ok']) && $_SESSION['adm_reg_ok'] == true)
{
    if (isset($_GET['step']) && $_GET['step'] == '5')
    {
        include_once H . 'install/inc/updateSet.php';
        if (save_settings($tmp_set))
        {
            unset($_SESSION['install_step'], $_SESSION['host'], $_SESSION['user'], $_SESSION['pass'],
                $_SESSION['db'], $_SESSION['adm_reg_ok'], $_SESSION['mysql_ok']);
        } else  $msg['Невозможно сохранить настройки системы'];
    }
} elseif (isset($_POST['reg']))
{

    if (!preg_match("#^([A-zА-я0-9\-\_\ ])+$#ui", $_POST['nick'])) $err[] =
            'В нике присутствуют запрещенные символы';
    if (preg_match("#[a-z]+#ui", $_POST['nick']) && preg_match("#[а-я]+#ui", $_POST['nick'])) 
            $err[] = 'Разрешается использовать символы только русского или только английского алфавита';
    if (preg_match("#(^\ )|(\ $)#ui", $_POST['nick'])) $err[] =
            'Запрещено использовать пробел в начале и конце ника';
    else
    {
        if (strlen2($_POST['nick']) < 3) $err[] = 'Ник короче 3-х символов';
        elseif (strlen2($_POST['nick']) > 16) $err[] = 'Ник длиннее 16-ти символов';
        elseif (mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `nick` = '" .
            mysql_real_escape_string($_POST['nick']) . "' LIMIT 1"), 0) != 0) $err[] =
                'Выбранный ник уже занят другим пользователем';
        else  $nick = $_POST['nick'];
    }

    if (!isset($_POST['password']) || $_POST['password'] == null) $err[] =
            'Введите пароль';
    else
    {
        if (strlen2($_POST['password']) < 6) $err[] = 'Пароль короче 6-ти символов';
        elseif (strlen2($_POST['password']) > 16) $err[] =
                'Пароль длиннее 16-ти символов';
        elseif (!isset($_POST['password_retry'])) $err[] =
                'Введите подтверждение пароля';
        elseif ($_POST['password'] !== $_POST['password_retry']) $err[] =
                'Пароли не совпадают';
        else  $password = $_POST['password'];
    }
    if (!isset($_POST['pol']) || !is_numeric($_POST['pol']) || ($_POST['pol'] !==
        '0' && $_POST['pol'] !== '1')) $err[] = 'Ошибка при выборе пола';
    else  $pol = intval($_POST['pol']);
    if (!isset($err))
    {
        mysql_query("
        INSERT INTO `user` (`nick`, `pass`, `date_reg`, `date_aut`, `date_last`, `pol`, `level`, `group_access`, `balls`)
        VALUES('" . $nick . "', '" . shif($password) . "', " . $time . ", " . $time . ", " . $time . ", '" . $pol . "', '4', '15', '10000')");
        $user = mysql_fetch_assoc(
        mysql_query("
        SELECT * FROM `user` 
        WHERE `nick` = '" . $nick . "' 
        AND `pass` = '" . shif($password) . "' LIMIT 1"));
        $_SESSION['message'] = lang('Успешно');
        $_SESSION['id_user'] = $user['id'];

        setcookie('id_user', $user['id'], time() + 60 * 60 * 24 * 365, '/', $_SERVER['HTTP_HOST']);
        setcookie('pass', cookie_encrypt($password, $user['id']), time() + 60 * 60 * 24 * 365, '/', $_SERVER['HTTP_HOST']);
        $_SESSION['adm_reg_ok'] = true;

    }
}

ob_start();

if (isset($_SESSION['adm_reg_ok']) && $_SESSION['adm_reg_ok'] == true)
{
    echo "<div class='msg'>Регистрация администратора прошла успешно</div>\n";
    if (isset($msg))
    {
        foreach ($msg as $key => $value)
        {
            echo "<div class='msg'>$value</div>\n";
        }
    }
    include_once 'inc/restart.php';
    include_once H . 'install/inc/updateSet.php';
    if (save_settings($tmp_set))
    {
    }

    if ($_SERVER["SERVER_ADDR"] != '127.0.0.1') delete_dir(H . 'install/');

?>
<script type="text/javascript">
   document.location.href = "/";
</script><noscript><div class="p_m"><a href="/">На сайт</a></div></noscript>
   <?

} else
{
    if (isset($err))
    {
        foreach ($err as $key => $value)
        {
            echo "<div class='err'>$value</div>\n";
        }
    }
    include_once 'inc/updateSetForm.php';

?>
                *Все поля обязательны к заполнению<br />
            <input type="submit" class="links" name="reg" value="Регистрация" /><br />
        </fieldset>
    </form>
</div>
<?

}

?>